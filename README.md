PHS
====================

Price history storage service.

### Running

* `docker-compose up -d`

* `docker-compose exec php bash`

    * `composer install`
    * `bin/console doctrine:migrations:migrate`

### Documentation

* Postman

    https://documenter.getpostman.com/view/8776767/SVmsTzJd?version=latest

* OpenAPI

    https://app.swaggerhub.com/apis-docs/dvlpm/PHS/1.0.0
