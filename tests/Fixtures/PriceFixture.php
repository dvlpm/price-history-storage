<?php declare(strict_types=1);

namespace App\Tests\Fixtures;

use App\Entity\Price;
use App\Entity\Product;
use App\Entity\Source;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class PriceFixture extends AbstractFixture implements DependentFixtureInterface
{
    protected function getEntities(): iterable
    {
        $source = $this->getReferenceByPrefixAndIndex(Source::class, 0);
        $source1 = $this->getReferenceByPrefixAndIndex(Source::class, 1);
        $product = $this->getReferenceByPrefixAndIndex(Product::class, 0);

        yield (new Price())
            ->setValue(12)
            ->setCapturedAt(new \DateTimeImmutable('1.01.2019'))
            ->setSource($source)
            ->setProduct($product);
        yield (new Price())
            ->setValue(10)
            ->setCapturedAt(new \DateTimeImmutable())
            ->setSource($source1)
            ->setProduct($product);
    }

    protected function getReferencePrefix(): string
    {
        return Price::class;
    }

    public function getDependencies(): array
    {
        return [
            ProductFixture::class,
            SourceFixture::class
        ];
    }
}
