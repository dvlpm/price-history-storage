<?php declare(strict_types=1);

namespace App\Tests\Fixtures;

use App\Entity\Product;

class ProductFixture extends AbstractFixture
{
    protected function getReferencePrefix(): string
    {
        return Product::class;
    }

    protected function getEntities(): iterable
    {
        foreach ([
                     'd7382fe2-e566-4e0e-a438-70b4a454c1a9' => 100,
                     '0160c5de-83d0-4f9f-9c27-9def09907c1b' => 200,
                 ] as $uuid => $priority) {
            yield (new Product())
                ->setUuid($uuid)
                ->setIsActive(true)
                ->setPriority($priority);
        }
    }
}
