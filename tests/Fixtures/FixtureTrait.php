<?php declare(strict_types=1);

namespace App\Tests\Fixtures;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\SchemaTool;

trait FixtureTrait
{
    protected function loadFixtures(FixtureInterface... $fixtures): void
    {
        $em = $this->getEntityManager();
        $metadata = $em->getMetadataFactory()->getAllMetadata();
        $schemaTool = new SchemaTool($em);
        $schemaTool->dropSchema($metadata);
        $schemaTool->createSchema($metadata);
        $purger = new ORMPurger($em);
        $purger->setPurgeMode(ORMPurger::PURGE_MODE_TRUNCATE);
        $executor = new ORMExecutor($em, $purger);
        foreach ($fixtures as $fixture) {
            if ($fixture instanceof DependentFixtureInterface) {
                $fixtures = array_merge(
                    $this->recursiveGetDependencies($fixture),
                    $fixtures
                );
            }
        }
        $executor->execute($fixtures, true);
    }

    private function recursiveGetDependencies(DependentFixtureInterface $dependentFixture): array
    {
        $fixtures = [];
        foreach ($dependentFixture->getDependencies() as $dependency) {
            $fixtures[] = $fixture = new $dependency();
            if ($fixture instanceof DependentFixtureInterface) {
                $fixtures = array_merge($fixtures, $this->recursiveGetDependencies($fixture));
            }
        }

        return $fixtures;
    }

    abstract protected function getEntityManager(): EntityManager;
}
// @TODO add to package
