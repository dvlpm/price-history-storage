<?php declare(strict_types=1);

namespace App\Tests\Fixtures;

use App\Entity\Source;

class SourceFixture extends AbstractFixture
{
    protected function getEntities(): iterable
    {
        yield (new Source())
            ->setDomain('market.yandex.ru');
        yield (new Source())
            ->setDomain('beru.ru');
    }

    protected function getReferencePrefix(): string
    {
        return Source::class;
    }
}
