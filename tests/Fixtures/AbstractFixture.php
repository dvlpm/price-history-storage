<?php declare(strict_types=1);

namespace App\Tests\Fixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

abstract class AbstractFixture extends Fixture
{
    public static array $entities;

    public function load(ObjectManager $manager): void
    {
        foreach ($this->getEntities() as $index => $entity) {
            $manager->persist($entity);
            $this->addReference($this->getReferencePrefix() . "-$index", $entity);
            static::$entities[$this->getReferencePrefix() . "-$index"] = $entity;
        }

        $manager->flush();
    }

    public static function getEntityByPrefixAndIndex(string $prefix, int $index): object
    {
        return static::$entities["$prefix-$index"];
    }

    protected function getReferenceByPrefixAndIndex(string $prefix, int $index): object
    {
        return $this->getReference($prefix . "-$index");
    }

    abstract protected function getEntities(): iterable;
    abstract protected function getReferencePrefix(): string;
}
// @TODO add to package
