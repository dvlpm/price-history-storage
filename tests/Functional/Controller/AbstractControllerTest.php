<?php declare(strict_types=1);

namespace App\Tests\Functional\Controller;

use App\Tests\Fixtures\FixtureTrait;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

abstract class AbstractControllerTest extends WebTestCase
{
    use FixtureTrait;

    protected KernelBrowser $client;
    protected ReferenceRepository $referenceRepository;

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->referenceRepository = new ReferenceRepository(static::$container->get(ObjectManager::class));
    }

    protected function getEntityManager(): EntityManager
    {
        return self::$container->get('doctrine.orm.entity_manager');
    }

    protected function assertResponseContentArrayEqual(array $expectedResponseArray): void
    {
        $response = $this->client->getResponse();

        $contentArray = json_decode($response->getContent(), true);

        $this->assertEquals($contentArray, $expectedResponseArray);
    }
}
// @TODO add to package
