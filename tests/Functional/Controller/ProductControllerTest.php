<?php declare(strict_types=1);

namespace App\Tests\Functional\Controller;

use App\Entity\Source;
use App\Tests\Fixtures\AbstractFixture;
use App\Tests\Fixtures\PriceFixture;
use App\Tests\Resources\Responses;
use Symfony\Component\HttpFoundation\Request;

class ProductControllerTest extends AbstractControllerTest
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->loadFixtures(new PriceFixture());
    }

    public function testOneHasExpiredPriceAndAnotherHasNoPriceAtAll(): void
    {
        /** @var Source $source */
        $source = AbstractFixture::getEntityByPrefixAndIndex(Source::class, 0);
        $this->client->request(
            Request::METHOD_GET,
            'source/' . $source->getDomain() . '/products-for-update'
        );
        $this->assertResponseContentArrayEqual(Responses::LIST_PRODUCTS_FOR_UPDATE);
    }

    public function testOneHasPriceAndAnotherHasUpToDatePrice(): void
    {
        /** @var Source $source1 */
        $source1 = AbstractFixture::getEntityByPrefixAndIndex(Source::class, 1);
        $this->client->request(
            Request::METHOD_GET,
            'source/' . $source1->getDomain() . '/products-for-update'
        );
        $this->assertResponseContentArrayEqual(Responses::LIST_PRODUCTS_FOR_UPDATE_1);
    }

    public function testNotExistedSource(): void
    {
        $this->client->request(
            Request::METHOD_GET,
            'source/not-yet-existed-source/products-for-update'
        );
        $this->assertResponseContentArrayEqual(Responses::LIST_PRODUCTS_FOR_UPDATE);
    }
}
