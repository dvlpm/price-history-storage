<?php declare(strict_types=1);

namespace App\Tests\Resources;

class Responses
{
    public const LIST_PRODUCT_PRICES_SUCCESS = [
        [
            'id' => 1,
            'price' => 12,
            'discount' => null,
            'product' =>
                [
                    'uuid' => 'd7382fe2-e566-4e0e-a438-70b4a454c1a9',
                ],
            'source' =>
                [
                    'id' => 1,
                    'domain' => 'market.yandex.ru',
                ],
            'capturedAt' => '2019-01-01T00:00:00+00:00',
        ],
    ];

    public const LIST_PRODUCTS_FOR_UPDATE = [
        ['uuid' => '0160c5de-83d0-4f9f-9c27-9def09907c1b'],
        ['uuid' => 'd7382fe2-e566-4e0e-a438-70b4a454c1a9'],
    ];

    public const LIST_PRODUCTS_FOR_UPDATE_1 = [
        ['uuid' => '0160c5de-83d0-4f9f-9c27-9def09907c1b']
    ];
}
