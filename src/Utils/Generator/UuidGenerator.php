<?php

namespace App\Utils\Generator;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;
use Ramsey\Uuid\Uuid;

class UuidGenerator extends AbstractIdGenerator
{
    public function generate(EntityManager $em, $entity): string
    {
        if (method_exists($entity, 'getUuid') && $uuid = $entity->getUuid()) {
            return $uuid;
        }

        return Uuid::uuid4()->toString();
    }
}
