<?php

declare(strict_types=1);

namespace App\Utils\Events;

use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ObjectEventBuilder
{
    private NormalizerInterface $normalizer;
    private ?object $object;
    private ?string $name;
    private ?array $groups;

    public function __construct(NormalizerInterface $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    public function reset(): self
    {
        $this->object = null;
        $this->name = null;
        $this->groups = null;

        return $this;
    }

    public function setObject(object $object): self
    {
        $this->object = $object;

        return $this;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function setGroups(string... $groups): self
    {
        foreach ($groups as $group) {
            $this->groups[] = $group;
        }

        return $this;
    }

    public function build(): Event
    {
        assert($this->name !== null);
        assert($this->object !== null);
        assert($this->groups !== null);

        return new Event(
            $this->name,
            $this->normalizer->normalize($this->object, get_class($this->object), [
                AbstractNormalizer::GROUPS => $this->groups,
            ]),
            new \DateTimeImmutable()
        );
    }
}
