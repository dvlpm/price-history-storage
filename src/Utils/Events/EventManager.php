<?php

declare(strict_types=1);

namespace App\Utils\Events;

use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

class EventManager
{
    /** @var Event[] */
    private array $events = [];

    private ProducerInterface $producer;
    private SerializerInterface $serializer;

    public function __construct(ProducerInterface $producer, SerializerInterface $serializer)
    {
        $this->producer = $producer;
        $this->serializer = $serializer;
    }

    public function beginCollecting(): void
    {
        $this->events = [];
    }

    public function collect(Event $event): void
    {
        $this->events[] = $event;
    }

    public function release(): void
    {
        foreach ($this->events as $event) {
            $message = $this->serializer->serialize($event, JsonEncoder::FORMAT);
            $this->producer->publish($message, $event->getName(), [
                'content_type' => 'application/json',
            ]);
        }
    }

    public function reset(): void
    {
        $this->beginCollecting();
    }
}
