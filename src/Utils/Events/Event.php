<?php

declare(strict_types=1);

namespace App\Utils\Events;

class Event
{
    private string $name;
    private array $data;
    private ?\DateTimeImmutable $occurredAt;

    public function __construct(string $name, array $data = [], \DateTimeImmutable $occurredAt = null)
    {
        $this->name = $name;
        $this->data = $data;
        $this->occurredAt = $occurredAt;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function getOccurredAt(): \DateTimeImmutable
    {
        return $this->occurredAt;
    }
}
