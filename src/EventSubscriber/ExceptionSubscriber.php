<?php

namespace App\EventSubscriber;

use App\Utils\Validation\ValidationException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class ExceptionSubscriber implements EventSubscriberInterface
{
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getException();

        if ($exception instanceof ValidationException) {
            $response = $this->getResponseForValidationException($exception);
        } else if ($exception instanceof HttpException) {
            $response = $this->getResponseForHttpException($exception);
        }

        $event->setResponse(
            $response ?? $this->createResponse(
                JsonResponse::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'exception' => [
                        'message' => $exception->getMessage(),
                        'class' => get_class($exception),
                        'trace' => $exception->getTrace()
                    ],
                ]
            )
        );
    }

    private function getResponseForValidationException(ValidationException $validatorException): JsonResponse
    {
        $messages = [];
        foreach ($validatorException->getViolationList() as $violation) {
            $messages[$violation->getPropertyPath()][] = $violation->getMessage();
        }

        return $this->createResponse(JsonResponse::HTTP_UNPROCESSABLE_ENTITY, $messages);
    }

    private function getResponseForHttpException(HttpException $httpException): JsonResponse
    {
        return $this->createResponse(
            $httpException->getStatusCode(),
            ['message' => $httpException->getMessage()]
        );
    }

    private function createResponse(int $code, array $messages): JsonResponse
    {
        return new JsonResponse(compact('code', 'messages'), $code);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException',
        ];
    }
}
