<?php

namespace App\Service;

use App\Entity\Price;
use App\Utils\Events\ObjectEventBuilder;
use App\Repository\PriceWriteRepository;
use App\Utils\Events\EventManager;
use App\Utils\Validation\Validator;

class PriceService
{
    private PriceWriteRepository $priceWriteRepository;
    private Validator $validator;
    private EventManager $eventManager;
    private ObjectEventBuilder $objectEventBuilder;

    public function __construct(
        PriceWriteRepository $priceWriteRepository,
        Validator $validator,
        EventManager $eventManager,
        ObjectEventBuilder $objectEventBuilder
    ) {
        $this->priceWriteRepository = $priceWriteRepository;
        $this->validator = $validator;
        $this->eventManager = $eventManager;
        $this->objectEventBuilder = $objectEventBuilder;
    }

    public function create(Price $price): void
    {
        $this->validator->validate($price);
        $this->priceWriteRepository->beginTransaction();
        $this->eventManager->beginCollecting();
        try {
            $this->priceWriteRepository->save($price);
            $this->eventManager->collect(
                $this->objectEventBuilder
                    ->reset()
                    ->setName('price.created')
                    ->setObject($price)
                    ->setGroups('price')
                    ->build()
            );

            $this->priceWriteRepository->commit();
            $this->eventManager->release();
        } catch (\Exception $exception) {
            $this->eventManager->reset();
            $this->priceWriteRepository->rollback();
        }
    }
}
