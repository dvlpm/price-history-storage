<?php

namespace App\Serializer;

use App\Entity\Source;
use App\Repository\SourceReadRepository;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class SourceByDomainDenormalizer implements DenormalizerInterface
{
    private SourceReadRepository $sourceReadRepository;

    private ?Source $source = null;

    public function __construct(
        SourceReadRepository $sourceReadRepository
    ) {
        $this->sourceReadRepository = $sourceReadRepository;
    }

    public function supportsDenormalization($data, $type, $format = null, $context = null): bool
    {
        if (($domain = $data['domain'] ?? null) === null) {
            return false;
        }

        $this->source = $this->sourceReadRepository->findOneByDomain($domain);

        return $this->source !== null;
    }

    public function denormalize($data, $class, $format = null, array $context = []): ?object
    {
        return $this->source;
    }
}
