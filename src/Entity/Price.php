<?php declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 */
class Price
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("price")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="float")
     * @Groups("price")
     * @Assert\NotBlank()
     */
    private ?float $value = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups("price")
     * @Assert\Type("integer")
     */
    private ?int $discount = null;

    /**
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="prices", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false, name="product_uuid", referencedColumnName="uuid")
     * @Groups("price")
     * @Assert\Valid()
     * @Assert\NotNull()
     */
    private ?Product $product = null;

    /**
     * @ORM\ManyToOne(targetEntity="Source", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups("price")
     * @Assert\Valid()
     * @Assert\NotNull()
     */
    private ?Source $source = null;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @Groups("price")
     * @Assert\NotBlank()
     */
    private \DateTimeImmutable $capturedAt;

    public function __construct()
    {
        $this->capturedAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(float $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getDiscount(): ?int
    {
        return $this->discount;
    }

    public function setDiscount(?int $discount): self
    {
        $this->discount = $discount;

        return $this;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): Price
    {
        $this->product = $product;

        return $this;
    }

    public function getSource(): Source
    {
        return $this->source;
    }

    public function setSource(Source $source): Price
    {
        $this->source = $source;

        return $this;
    }

    public function getCapturedAt(): \DateTimeImmutable
    {
        return $this->capturedAt;
    }

    public function setCapturedAt(\DateTimeImmutable $capturedAt): Price
    {
        $this->capturedAt = $capturedAt;

        return $this;
    }
}
