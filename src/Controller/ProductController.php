<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductReadRepository;
use App\Service\ProductService;
use App\Utils\Validation\ValidationException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

class ProductController extends AbstractController
{
    /**
     * @Route("products", methods={"GET"})
     *x
     * @param ProductReadRepository $readRepository
     * @param array $orderBy
     * @param int|null $limit
     * @param int|null $offset
     * @return JsonResponse
     */
    public function list(
        ProductReadRepository $readRepository,
        array $orderBy = [],
        int $limit = null,
        int $offset = null
    ): JsonResponse {
        return $this->json(
            $readRepository->findBy([], $orderBy, $limit, $offset),
            JsonResponse::HTTP_OK,
            [],
            [AbstractNormalizer::GROUPS => 'product']
        );
    }

    /**
     * @Route("source/{sourceDomain}/products-for-update")
     *
     * @param ProductReadRepository $readRepository
     * @param string $sourceDomain
     * @param int|null $limit
     * @param int|null $offset
     * @return JsonResponse
     * @throws \Exception
     */
    public function listForUpdate(
        ProductReadRepository $readRepository,
        string $sourceDomain,
        int $limit = null,
        int $offset = null
    ): JsonResponse {
        return $this->json(
            $readRepository->findForUpdateBySourceDomain($sourceDomain, $limit, $offset),
            JsonResponse::HTTP_OK,
            [],
            [AbstractNormalizer::GROUPS => 'product']
        );
    }

    /**
     * @Route("/products", methods={"POST"})
     *
     * @param ProductService $productService
     * @param Product $product
     * @return JsonResponse
     * @throws ValidationException
     */
    public function create(ProductService $productService, Product $product): JsonResponse
    {
        $productService->create($product);

        return $this->json($product, JsonResponse::HTTP_OK, [], [
            AbstractNormalizer::GROUPS => 'product',
        ]);
    }
}
