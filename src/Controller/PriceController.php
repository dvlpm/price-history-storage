<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Price;
use App\ParameterObject\PriceFilterBag;
use App\Repository\PriceReadRepository;
use App\Service\PriceService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use App\Utils\Validation\ValidationException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class PriceController extends AbstractController
{
    /**
     * @Route("products/{productUuid}/prices")
     *
     * @param PriceReadRepository $readRepository
     * @param string $productUuid
     * @param string $sourceDomain
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function list(
        PriceReadRepository $readRepository,
        string $productUuid,
        string $sourceDomain = null,
        array $orderBy = [],
        int $limit = null,
        int $offset = null
    ): JsonResponse {
        return $this->json(
            $readRepository->findByFilterBag(
                new PriceFilterBag($productUuid, $sourceDomain),
                $orderBy,
                $limit,
                $offset
            ),
            JsonResponse::HTTP_OK,
            [],
            [AbstractNormalizer::GROUPS => 'price']
        );
    }

    /**
     * @Route("prices", methods={"POST"})
     *
     * @param PriceService $service
     * @param Price $price
     * @return JsonResponse
     * @throws ValidationException
     * @throws ExceptionInterface
     */
    public function create(PriceService $service, Price $price): JsonResponse
    {
        $service->create($price);

        return $this->json($price, JsonResponse::HTTP_OK, [], [
            AbstractNormalizer::GROUPS => 'price',
        ]);
    }
}
