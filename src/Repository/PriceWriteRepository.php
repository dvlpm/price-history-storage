<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Price;

class PriceWriteRepository extends AbstractWriteRepository
{
    public function save(Price $price): void
    {
        $this->doSave($price);
    }
}
