<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Source;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityNotFoundException;

/**
 * @method Source|null find($id, $lockMode = null, $lockVersion = null)
 * @method Source|null findOneBy(array $criteria, array $orderBy = null)
 * @method Source[]    findAll()
 * @method Source[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SourceReadRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Source::class);
    }
    
    public function findOneByDomain(string $domain): ?Source
    {
        return $this->findOneBy(['domain' => $domain]);
    }

    public function findOneByDomainOrFail(string $domain): Source
    {
        $source = $this->findOneByDomain($domain);
        
        if ($source === null) {
            throw new EntityNotFoundException(Source::class);
        }
        
        return $source;
    }
}
