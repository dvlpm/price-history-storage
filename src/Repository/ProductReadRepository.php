<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NativeQuery;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductReadRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @param string $sourceDomain
     * @param null $limit
     * @param null $offset
     * @return Product[]
     * @throws \Exception
     */
    public function findForUpdateBySourceDomain(string $sourceDomain, $limit = null, $offset = null): array
    {
        $expiredAt = (new \DateTimeImmutable('yesterday'))->format('Y-m-d');
        $query = $this->createFindForUpdateBySourceDomainNativeQuery()
            ->setParameter('expiredAt', $expiredAt)
            ->setParameter('sourceDomain', $sourceDomain)
            ->setParameter('limit', $limit ?: 15)
            ->setParameter('offset', $offset ?: 0);

        return $query->execute();
    }

    private function createFindForUpdateBySourceDomainNativeQuery(): NativeQuery
    {
        $rsm = new ResultSetMappingBuilder($this->getEntityManager());
        $rsm->addRootEntityFromClassMetadata(Product::class, 'p');

        return $this->getEntityManager()->createNativeQuery(
            $this->createFindForUpdateBySourceDomainSqlString(),
            $rsm
        );
    }

    private function createFindForUpdateBySourceDomainSqlString(): string
    {
        return <<<SQL
            SELECT DISTINCT product.*
            FROM product
                     LEFT JOIN price ON price.product_uuid = product.uuid
                     LEFT JOIN source ON price.source_id = source.id AND source.domain = :sourceDomain
                     LEFT JOIN (
                SELECT product_uuid, source_id, MAX(captured_at) as max_captured_at
                FROM price
                         LEFT JOIN source s on price.source_id = s.id
                WHERE s.domain = :sourceDomain
                GROUP BY product_uuid, source_id
            ) as last_price ON last_price.product_uuid = product.uuid
            WHERE product.is_active = true
              AND (max_captured_at < :expiredAt OR max_captured_at IS NULL)
              AND (source.domain = :sourceDomain OR source.domain IS NULL)
            ORDER BY product.priority DESC
            LIMIT :limit
            OFFSET :offset
        SQL;
    }
}
