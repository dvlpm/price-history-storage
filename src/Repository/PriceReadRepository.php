<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Price;
use App\ParameterObject\PriceFilterBag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Price|null find($id, $lockMode = null, $lockVersion = null)
 * @method Price|null findOneBy(array $criteria, array $orderBy = null)
 * @method Price[]    findAll()
 * @method Price[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PriceReadRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Price::class);
    }

    /**
     * @param PriceFilterBag $filterBag
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     * @return Price[]
     * @throws \Doctrine\ORM\ORMException
     */
    public function findByFilterBag(
        PriceFilterBag $filterBag,
        array $orderBy = [],
        int $limit = null,
        int $offset = null
    ): array {
        $queryBuilder = $this->createQueryBuilder('price')
            ->leftJoin('price.source', 'source')
            ->leftJoin('price.product', 'product')
            ->where('product.uuid = :productUuid')
            ->setParameter('productUuid', $filterBag->getProductUuid())
            ->setMaxResults($limit)
            ->setFirstResult($offset);

        if ($filterBag->hasSourceDomain()) {
            $queryBuilder->andWhere('source.domain = :sourceDomain')
                ->setParameter('sourceDomain', $filterBag->getSourceDomain());
        }

        foreach ($orderBy as $sort => $order) {
            $queryBuilder->addOrderBy("price.$sort", $order);
        }

        return $queryBuilder->getQuery()->execute();
    }
}
