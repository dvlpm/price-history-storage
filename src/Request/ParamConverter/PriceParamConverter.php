<?php

namespace App\Request\ParamConverter;

use App\Entity\Price;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class PriceParamConverter extends AbstractObjectParamConverter
{
    protected array $groups = ['price'];

    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === Price::class;
    }

    protected function extractContentArrayFromRequest(Request $request): array
    {
        $contentArray = parent::extractContentArrayFromRequest($request);

        $contentArray['value'] = (float) ($contentArray['value'] ?? 0);

        return $contentArray;
    }


}
