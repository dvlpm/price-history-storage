<?php

namespace App\Request\ParamConverter;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

abstract class AbstractObjectParamConverter implements ParamConverterInterface
{
    private DenormalizerInterface $denormalizer;
    /** @var string[] */
    protected array $allowedMethods = [Request::METHOD_POST];
    /** @var string[] */
    protected array $groups = [];

    public function __construct(
        DenormalizerInterface $denormalizer
    ) {
        $this->denormalizer = $denormalizer;
    }

    /**
     * @param Request $request
     * @param ParamConverter $configuration
     * @return bool
     * @throws ExceptionInterface
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {
        if (!in_array($request->getMethod(), $this->getAllowedMethods(), true)) {
            return false;
        }

        $groups = $this->getGroups($configuration);
        $contentArray = $this->extractContentArrayFromRequest($request);
        $objectToPopulate = $this->getObjectToPopulate($request, $configuration);

        $request->attributes->set($configuration->getName(), $this->denormalizer->denormalize(
            $contentArray,
            $configuration->getClass(),
            null,
            [
                AbstractNormalizer::GROUPS => $groups,
                AbstractNormalizer::OBJECT_TO_POPULATE => $objectToPopulate,
            ]
        ));

        return true;
    }

    protected function getAllowedMethods(): array
    {
        return $this->allowedMethods;
    }

    protected function extractContentArrayFromRequest(Request $request): array
    {
        return json_decode(
            $request->getContent(),
            true,
            512,
            JSON_THROW_ON_ERROR
        ) ?: [];
    }

    protected function getGroups(ParamConverter $configuration): array
    {
        return $configuration->getOptions()['groups'] ?? $this->groups;
    }

    protected function getObjectToPopulate(Request $request, ParamConverter $configuration): ?object
    {
        $class = $configuration->getClass();

        return new $class;
    }

    abstract public function supports(ParamConverter $configuration): bool;
}
