<?php

namespace App\ParameterObject;

class PriceFilterBag
{
    private string $productUuid;
    private ?string $sourceDomain;

    public function __construct(
        string $productUuid,
        ?string $sourceDomain
    ) {
        $this->productUuid = $productUuid;
        $this->sourceDomain = $sourceDomain;
    }

    public function getProductUuid(): string
    {
        return $this->productUuid;
    }

    public function getSourceDomain(): ?string
    {
        return $this->sourceDomain;
    }

    public function hasSourceDomain(): bool
    {
        return $this->sourceDomain !== null;
    }
}
