<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190910191953 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE price_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE price (
          id INT NOT NULL, 
          product_uuid UUID NOT NULL, 
          source_id INT NOT NULL, 
          value DOUBLE PRECISION NOT NULL, 
          discount INT DEFAULT NULL, 
          captured_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, 
          PRIMARY KEY(id)
        )');
        $this->addSql('CREATE INDEX IDX_7E8797685C977207 ON price (product_uuid)');
        $this->addSql('CREATE INDEX IDX_7E879768953C1C61 ON price (source_id)');
        $this->addSql('COMMENT ON COLUMN price.captured_at IS \'(DC2Type:date_immutable)\'');
        $this->addSql('ALTER TABLE 
          price 
        ADD 
          CONSTRAINT FK_7E8797685C977207 FOREIGN KEY (product_uuid) REFERENCES product (uuid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE 
          price 
        ADD 
          CONSTRAINT FK_7E879768953C1C61 FOREIGN KEY (source_id) REFERENCES source (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE price_id_seq CASCADE');
        $this->addSql('DROP TABLE price');
    }
}
